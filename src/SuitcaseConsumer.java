import java.util.logging.Logger;

/**
 * Consumes {@link Suitcase} objects from a {@link Belt} at random intervals.
 *
 * @author Aram Kocharyan
 */
public class SuitcaseConsumer extends RandomBeltThread {

    private static Logger log =
            Logger.getLogger(SuitcaseConsumer.class.getName());
    private volatile boolean isWaiting;

    public SuitcaseConsumer(Belt belt) {
        super(belt);
        setInterval(4000);
    }

    public synchronized void execute() {
        try {
            isWaiting = true;
            Suitcase suitcase = getBelt().getEndBelt();
            if (!suitcase.isClean()) {
                throw new SuspiciousException("Non-cleaned suitcase");
            } else {
                log.info("Consumed");
                // Notify any component waiting for a consume
                isWaiting = false;
                notifyAll();
            }
        } catch (InterruptedException e) {
            return;
        } catch (SuspiciousException e) {
            NotificationService.getDefault().notifyException(e);
        }
    }

    /**
     * @return Whether the consumer is waiting for changes from the belt.
     */
    public boolean isWaiting() {
        return isWaiting;
    }

}
