import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Logger;

/**
 * A simple implementation of a {@link Belt}. Belt indices start from 0.
 *
 * @author Aram Kocharyan
 */
public class SimpleBelt extends IntervalThread implements Belt {

    private static Logger log =
            Logger.getLogger(SimpleBelt.class.getName());

    // TODO allow these to be modified in subclasses while hiding
    // implementation here (eg. use methods instead)
    private static final String MSG_INTERRUPTED = "Belt is interrupted";
    private static final String MSG_EXISTS = "Suitcase exists at position";
    private static final String MSG_FALL = "Suitcase about to fall off";

    // The decision to put this here was based on the presence of
    // getScanPos() and since the only reference to XRay was under the Belt
    // section of the doc.
    private XRay xRay;

    // Synchronised methods would still allow multiple threads to call two
    // methods in this class which modify the underlying belt - using a Lock
    // ensures only one of these methods can ever be running on any given
    // thread.
    private final Lock lock = new ReentrantLock();

    /**
     * The underlying belt. This is preferred to extending LinkedList since it
     * doesn't expose the underlying data structure, which other classes may
     * start to rely on.
     */
    private LinkedList<Suitcase> belt;

    /**
     * Keeps a list of the positions not containing suitcases.
     */
    private ArrayList<Integer> freePositions;

    /**
     * Whether the free positions array needs to be recalculated.
     */
    private boolean freePositionsDirty;

    public SimpleBelt() {
        belt = new LinkedList<Suitcase>();
        for (int i = 0; i < maxSize(); i++) {
            belt.add(0, null);
        }
        freePositionsDirty = true;
        freePositions();
        xRay = new SimpleXRay();
        setInterval(500);
    }

    /**
     * @return The {@link XRay} used for detecting suspicious {@link Suitcase}
     * objects.
     */
    public XRay getXRay() {
        return xRay;
    }

    /**
     * {@link SimpleBelt#getXRay()}
     */
    public void setXRay(XRay xRay) {
        this.xRay = xRay;
    }

    @Override
    public int maxSize() {
        // NOTE: this couples the class to Position
        return Position.values().length - 1;
    }

    @Override
    public int getScanPos() {
        return Position.THREE.ordinal() - 1;
    }

    @Override
    public int getEndPos() {
        return maxSize() - 1;
    }

    @Override
    public Suitcase peek(int index) {
        return belt.get(index);
    }

    @Override
    public Suitcase peekStart() {
        return peek(0);
    }

    @Override
    public Suitcase peekEnd() {
        return peek(getEndPos());
    }

    @Override
    public Suitcase peekScanPos() {
        return peek(getScanPos());
    }

    @Override
    public int freePositionCount() {
        return freePositions().size();
    }

    @Override
    public List<Integer> freePositions() {
        if (freePositionsDirty) {
            freePositions = new ArrayList<Integer>();
            for (int i = 0; i < belt.size(); i++) {
                Suitcase suitcase = belt.get(i);
                if (suitcase == null) {
                    freePositions.add(i);
                }
            }
            freePositionsDirty = false;
        }
        return freePositions;
    }

    @Override
    public Integer lastFreePosition() {
        List<Integer> lastFree = freePositions();
        return lastFree.size() > 0 ? lastFree.get(lastFree.size() - 1) : null;
    }

    @Override
    public boolean needScan() {
        try {
            xRay.xRay(peekScanPos());
        } catch (InterruptedException e) {
            return false;
        }
        return xRay.needScan();
    }

    /**
     * Precondition: getEndPos() < belt.size()
     * @throws InterruptedException
     * @throws OverloadException
     */
    @Override
    public synchronized void move() throws InterruptedException,
            OverloadException {
        if (this.isInterrupted()) {
            throw new InterruptedException(MSG_INTERRUPTED);
        } else if (belt.get(getEndPos()) != null) {
            // We must not damage suitcases when avoidable
            throw new OverloadException(MSG_FALL);
        } else {
            lock.lock();
            // Stop the belt and notify of a move
            this.setExecuting(false);
            belt.addFirst(null);
            belt.removeLast();
            freePositionsDirty = true;
            log.info("Belt moved");
            notifyAll();
            lock.unlock();
        }
    }

    /**
     * Same function as {@link Belt#put(Suitcase, int)} but performs it without
     * waiting for the position to become available.
     *
     * @throws InterruptedException If the thread executing is interrupted.
     * @throws OverloadException If there is a suitcase at the position.
     */
    private synchronized void set(Suitcase suitcase, int position) throws
            InterruptedException, OverloadException {
        if (this.isInterrupted()) {
            throw new InterruptedException(MSG_INTERRUPTED);
        } else {
            lock.lock();
            if (belt.get(position) != null && suitcase != null) {
                lock.unlock();
                // We must not damage suitcases when avoidable
                throw new OverloadException(MSG_EXISTS + " " + position);
            } else {
                this.setExecuting(false);
                belt.set(position, suitcase);
                freePositionsDirty = true;
                notifyAll();
            }
            lock.unlock();
        }
    }

    @Override
    public synchronized void put(Suitcase suitcase, int position) throws InterruptedException, OverloadException {
        if (this.isInterrupted()) {
            throw new InterruptedException(MSG_INTERRUPTED);
        } else {
            while (peek(position) != null) {
                log.info("Waiting for position " + position + " to be freed");
                wait();
            }
            set(suitcase, position);
        }
    }

    @Override
    public synchronized Suitcase get() throws InterruptedException {
        return getPosition(getScanPos());
    }

    @Override
    public synchronized Suitcase getEndBelt() throws InterruptedException {
        return getPosition(getEndPos());
    }

    /**
     * @return The Suitcase at the given position.
     * @throws InterruptedException
     */
    public synchronized Suitcase getPosition(int position) throws InterruptedException {
        // NOTE: the stop position isn't documented anywhere - do you mean scan?
        if (this.isInterrupted()) {
            throw new InterruptedException(MSG_INTERRUPTED);
        } else {
            while (peek(position) == null) {
                log.info("Wait for position to exist: " + position);
                wait();
            }
            log.info("Done waiting for position to exist: " + position);
            lock.lock();
            Suitcase suitcase = belt.get(position);
            belt.set(position, null);
            freePositionsDirty = true;
            lock.unlock();
            return suitcase;
        }
    }

    @Override
    public void execute() {
        try {
            move();
        } catch (Exception e) {
            e.printStackTrace();
            NotificationService.getDefault().notifyException(e);
        }
    }

}
