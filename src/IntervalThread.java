import java.util.logging.Logger;

/**
 * A Thread which executes an action at intervals.
 *
 * @author Aram Kocharyan
 */
public abstract class IntervalThread extends Thread {

    private volatile boolean executing = true;
    private volatile long interval = 3000;
    private static Logger log =
            Logger.getLogger(IntervalThread.class.getName());

    public void run() {
        while (true) {
            try {
                Thread.sleep(getInterval());
                if (isExecuting()) {
                    execute();
                }
            } catch (InterruptedException e) {
                log.info("Thread already sleeping");
                Thread.currentThread().interrupt();
                break;
            }
        }
    }

    public void interrupt() {
        super.interrupt();
        setExecuting(false);
    };

    /**
     * @return Whether the {@link IntervalThread#execute()} method is being
     * called.
     */
    public boolean isExecuting() {
        return executing;
    }

    /**
     * {@link IntervalThread#isExecuting()}
     */
    public synchronized void setExecuting(boolean executing) {
        this.executing = executing;
    }

    /**
     * @return The wait time in milliseconds before executing.
     */
    public long getInterval() {
        return interval;
    }

    /**
     * {@link IntervalThread#getInterval()}
     */
    public synchronized void setInterval(long interval) {
        this.interval = interval;
    }

    /**
     * The body method of the execution which is called at intervals.
     */
    public abstract void execute();

}
