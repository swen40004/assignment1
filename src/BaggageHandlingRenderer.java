import javax.swing.*;
import java.awt.*;

/**
 * A Thread dedicated to rendering the baggage handing system.
 *
 * @author Aram Kocharyan
 */
public class BaggageHandlingRenderer extends IntervalThread {

    private JFrame frame;

    public BaggageHandlingRenderer(Belt belt,
                                   Scanner scanner,
                                   RobotArms robotArms) {
        try {
            frame = new JFrame();
            frame.add(new BaggageHandlingView(frame,
                    belt,
                    scanner,
                    robotArms));
            setInterval(50);
        } catch (HeadlessException e) {
            // No X11 DISPLAY. Allows other threads to function.
            e.printStackTrace();
            interrupt();
        }
    }

    public void execute() {
        frame.update(frame.getGraphics());
        frame.setVisible(true);
    }

}
