/**
 * An interface for the xray.
 *
 * @author Nicole Ronald
 */
interface XRay {
    /**
     * @param suitcase the suitcase to scan
     */
    void xRay(Suitcase suitcase) throws InterruptedException;

    /**
     * @return whether this suitcase requires further scanning
     */
    boolean needScan();

    /**
     * Resets the scanning flag to false.
     */
    void clearXRayFlag();

}
