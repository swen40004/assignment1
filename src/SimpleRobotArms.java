/**
 * A simple implementation of {@link RobotArms}.
 *
 * @author Aram Kocharyan
 */
public class SimpleRobotArms implements RobotArms {

    private static final String MSG_INTERRUPTED = "Arm is interrupted";
    private static final String MSG_CONTAINS = "Arm already contains a suitcase";
    Suitcase suitcase;

    // NOTE: coupled to Scanner - this should be called a null position instead
    Position position = Position.SCANNER;

    @Override
    public Position getPosition() {
        return position;
    }

    @Override
    public Suitcase getArmSuitcase() {
        return suitcase;
    }

    @Override
    public void pickup(Suitcase suitcase) throws OverloadException {
        if (this.suitcase != null) {
            throw new OverloadException(MSG_CONTAINS);
        } else {
            this.suitcase = suitcase;
        }
    }

    @Override
    public Suitcase drop() {
        Suitcase oldSuitcase = suitcase;
        suitcase = null;
        return oldSuitcase;
    }

    @Override
    public void move(Position pos) throws InterruptedException {
        position = pos;
    }

    @Override
    public void moveClockwise() throws InterruptedException {
        // TODO Ignored as per FAQ
    }

    @Override
    public void moveCounterclockwise() throws InterruptedException {
        // TODO Ignored as per FAQ
    }

}
