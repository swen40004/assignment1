/**
 * A simple implementation of a {@link Scanner}.
 *
 * @author Aram Kocharyan
 */
public class SimpleScanner implements Scanner {

    private Suitcase suitcase;

    @Override
    public Suitcase peek() {
        return suitcase;
    }

    @Override
    public void scan() throws InterruptedException, UnsuspiciousException {
        // This adds time to the scan operation,
        // makes it easier to visualise which suitcases are
        // being scanned - only used for testing
        if (suitcase != null) {
            if (!suitcase.isSuspicious()) {
                throw new UnsuspiciousException("Scanned suitcase isn't " +
                        "suspicious");
            } else {
                suitcase.clean();
            }
        }
    }

    @Override
    public Suitcase get() throws InterruptedException {
        Suitcase oldSuiteCase = suitcase;
        suitcase = null;
        return oldSuiteCase;
    }

    @Override
    public void put(Suitcase suitcase) throws OverloadException,
            UnsuspiciousException {
        if (this.suitcase != null) {
            throw new OverloadException("Suitcase already exists on scanner");
        } else {
            this.suitcase = suitcase;
        }
    }

}
