import java.util.Observable;
import java.util.Observer;
import java.util.logging.Logger;

/**
 * A controller for the baggage handling system which creates all components,
 * the view and manages their interactions.
 *
 * @author Aram Kocharyan
 */
public class BaggageHandlingController extends Thread implements Observer {

    private final SimpleBelt belt;
    private final Scanner scanner;
    private final RobotArms robotArms;
    private final SuitcaseProducer producer;
    private final SuitcaseConsumer consumer;
    private final BaggageHandlingRenderer renderer;

    /**
     * Whether a suitcase is being transitioned between the belt and scanner.
     */
    private volatile boolean inTransition;
    private volatile boolean delayed;

    // These are used for performing the scan separate from the controller code
    private final Runnable scanRun;
    private Thread scanThread;

    private static Logger log =
            Logger.getLogger(BaggageHandlingController.class.getName());

    public BaggageHandlingController() {
        NotificationService.getDefault().addObserver(this);
        belt = new SimpleBelt();

        scanner = new SimpleScanner();
        robotArms = new SimpleRobotArms();
        producer = new SuitcaseProducer(belt);
        consumer = new SuitcaseConsumer(belt);
        renderer = new BaggageHandlingRenderer(belt, scanner, robotArms);

        scanRun = new Runnable() {
            public void run() {
                try {
                    // Here it is assumed robotArms contains the suitcase
                    delay(100);
                    robotArms.move(Position.SCANNER);
                    delay(100);
                    scanner.put(robotArms.drop());
                    log.info("Scanning");
                    delay(500);
                    scanner.scan();
                    log.info("Finished scanning");
                    delay(100);
                    robotArms.pickup(scanner.get());

                    // Stop the belt to ensure free positions are correct
                    // and no movement takes place while adding suitcases
                    inTransition = true;
                    belt.setExecuting(false);

                    Integer last = belt.lastFreePosition();
                    if (last == null) {
                        throw new Exception("No free positions");
                    } else {
                        int ordinal = last + 1;
                        Position pos = Position.valueFromOrdinal(ordinal);
                        delay(100);
                        robotArms.move(pos);
                        log.info("Putting down");
                        delay(100);
                        robotArms.drop();
                        belt.put(robotArms.drop(), last);
                        inTransition = false;
                        log.info("Scanned and moved to " + pos);
                    }
                } catch (Exception e) {
                    NotificationService.getDefault().notifyException(e);
                }
            }
        };
        scanThread = new Thread(scanRun);
    }

    public void run() {
        while (true) {
            try {
                synchronized (belt) {
                    log.info("Waiting for belt");
                    belt.wait();
                }

                if (belt.needScan()) {
                    if (scanThread.isAlive()) {
                        // We are still scanning, and need to scan another,
                        // so we wait until the current scan is complete.
                        log.info("Already scanning, waiting until done");
                        scanThread.join();
                    }
                    if (!scanThread.isAlive()) {
                        log.info("Preparing to scan");
                        inTransition = true;
                        delay(100);
                        robotArms.move(Position.valueFromOrdinal(
                                belt.getScanPos() + 1));
                        delay(100);
                        robotArms.pickup(belt.get());
                        inTransition = false;
                        scanThread = new Thread(scanRun);
                        scanThread.start();
                    }
                }

                // Only wait for the consumer if it isn't waiting for the belt,
                // otherwise we could have a cyclic wait() and cause deadlock.
                if (!consumer.isWaiting()) {
                    log.info("Starting to wait for consumer");
                    synchronized (consumer) {
                        log.info("Waiting for consumer");
                        while (belt.peekEnd() != null) {
                            consumer.wait();
                        }
                    }
                }

                int free = belt.freePositionCount();

                while ((free == 1 && belt.peekEnd() == null &&
                        scanThread.isAlive()) || inTransition) {
                }
                // Continue running the belt unless the only free position
                // is at the end and we're scanning. This means it's perhaps
                // the only place left to place our cleaned suitcase when it
                // is finished scanning. This prevents the belt from
                // starting during transitions.
                log.info("Free positions: " + free);
                belt.setExecuting(true);

            } catch (Exception e) {
                // Handle any exceptions that take place
                NotificationService.getDefault().notifyException(e);
            }
        }
    }

    public void start() {
        super.start();
        belt.start();
        producer.start();
        consumer.start();
        renderer.start();
    }

    public void interrupt() {
        belt.interrupt();
        producer.interrupt();
        consumer.interrupt();
        renderer.interrupt();
        super.interrupt();
    }

    /**
     * @return Whether to delay components and add realism (for testing only).
     */
    public boolean isDelayed() {
        return delayed;
    }

    /**
     * {@link BaggageHandlingController#isDelayed()}
     */
    public void setDelayed(boolean delayed) {
        this.delayed = delayed;
    }

    /**
     * Helper method to sleep the current thread for a certain duration.
     * {@link BaggageHandlingController#isDelayed()} must be true.
     *
     * @param millis The time in milliseconds to sleep.
     */
    private void delay(long millis) {
        if (delayed) {
            try {
                Thread.sleep(millis);
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    @Override
    public void update(Observable o, Object arg) {
        if (arg instanceof Exception) {
            Exception e = (Exception) arg;
            e.printStackTrace();
            String msg = e.getMessage();
            if (msg != null) {
                log.severe(msg);
            }
            if (e instanceof BaggageHandlingException) {
                // I didn't exit the system because we may need to run
                // deallocation at some point, and also there may be other
                // threads running which we want to continue.
                this.interrupt();
                log.severe("Baggage system has halted!");
//                System.exit(1);
            }
        }
    }

}
