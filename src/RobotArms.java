/**
 * RobotArms implementations represent the robotic arms, which take
 * suitcases from the belt, place them on the scanner, and then onto
 * the belt.
 *
 * @author Tim Miller, Nicole Ronald
 */
interface RobotArms {
    /**
     * @return the current position of the robotic arms.
     */
    Position getPosition();

    /**
     * @return peek at the suitcase on the feed arm.
     */
    Suitcase getArmSuitcase();

    /**
     * Pick up a suitcase from the scanner.
     *
     * @param suitcase the suitcase to be picked up.
     * @throws OverloadException if there is already a suitcase in the arm.
     */
    public void pickup(Suitcase suitcase) throws OverloadException;

    /**
     * Drop the suitcase from the deposit arm.
     *
     * @return the suitcase that is dropped
     */
    Suitcase drop();


    /**
     * Move the arm.
     */
    void move(Position pos) throws InterruptedException;

    void moveClockwise() throws InterruptedException;

    void moveCounterclockwise() throws InterruptedException;

}

