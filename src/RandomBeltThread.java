/**
 * A Thread which executes random actions on a {@link Belt}.
 *
 * @author Aram Kocharyan
 */
public abstract class RandomBeltThread extends RandomThread {

    public RandomBeltThread(Belt belt) {
        setBelt(belt);
    }

    private Belt belt;

    /**
     * @return The underlying {@link Belt}
     */
    public Belt getBelt() {
        return belt;
    }

    /**
     * {@link RandomBeltThread#getBelt()}
     */
    public synchronized void setBelt(Belt belt) {
        this.belt = belt;
    }

}
