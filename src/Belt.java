import java.util.List;

/**
 * An interface for the deposit and feed belts.
 *
 * @author Tim Miller, Nicole Ronald
 */
interface Belt {
    /**
     * @return The size of this belt.
     */
    public int maxSize();

    /**
     * @return The position of the XRay.
     */
    public int getScanPos();

    /**
     * @return The final position on the belt
     */
    public int getEndPos();

    /**
     * Look at the value at a specified place on the belt.
     *
     * @param index the index at which to peek.
     * @return The suitcase on the scan position.
     */
    public Suitcase peek(int index);

    /**
     * @return The suitcase at the start position.
     */
    public Suitcase peekStart();

    /**
     * @return The suitcase at the start position.
     */
    public Suitcase peekEnd();

    /**
     * @return The suitcase at the scan position.
     */
    public Suitcase peekScanPos();

    /**
     * @return The number of positions not containing suitcases.
     */
    public int freePositionCount();

    /**
     * @return A List of free positions.
     */
    public List<Integer> freePositions();

    /**
     * @return The last free position.
     */
    public Integer lastFreePosition();

    /**
     * @return Whether a scan is needed for the suitcase at the scan position.
     */
    public boolean needScan();

    /**
     * Moves the belt along one place.
     */
    public void move()
            throws InterruptedException, OverloadException;

    /**
     * Put a suitcase on the belt.
     *
     * @param suitcase to put onto the belt.
     * @throws OverloadException    if there is a suitcase at the position.
     * @throws InterruptedException if the thread executing is interrupted.
     */

    // NOTE: I've modified this to say "position 0" from "position 1",
    // since since a Scanner isn't on the belt.
    void put(Suitcase suitcase, int position)
            throws InterruptedException, OverloadException;

    /**
     * Removes a suitcase from the scan position.
     *
     * @return The suitcase on the scan position.
     * @throws InterruptedException if the thread executing is interrupted.
     */
    Suitcase get() throws InterruptedException;

    /**
     * Removes a suitcase from the end of the belt.
     *
     * @return The suitcase on the scan position.
     * @throws InterruptedException if the thread executing is interrupted.
     */
    Suitcase getEndBelt() throws InterruptedException;

}
