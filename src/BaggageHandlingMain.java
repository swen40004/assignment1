import apple.laf.JRSUIUtils;

/**
 * The control component of the simulator.
 *
 * @author Tim Miller, Nicole Ronald, Aram Kocharyan
 */
public class BaggageHandlingMain {

    /**
     * Create the components for the factory, and start all of the
     * threads running. Create the UI and continue calling it's update
     * method.
     */
    public static void main(String[] args) {
        BaggageHandlingController baggage = new BaggageHandlingController();

        // Uncomment to add delayed movements for testing
        // baggage.setDelayed(true);

        Thread crash = new Thread() {
            public void run() {
                try {
                    Thread.sleep(3000);
                    NotificationService.getDefault().notifyException(
                            new OverloadException("Dummy Crash"));
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                }
            }
        };
        // Uncomment to simulate a system shutdown
        // crash.start();

        baggage.start();
    }

}
