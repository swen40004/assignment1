/**
 * A Scanner scans suitcases that has been labelled suspicious.
 *
 * @author Tim Miller, Nicole Ronald
 */
interface Scanner {
    /**
     * Peek at the suitcase on the scanner.
     */
    Suitcase peek();

    /**
     * Scan suitcase on the scanner.
     *
     * @throws UnsuspiciousException if the suitcase on the scanner is
     *                               already clean.
     */
    void scan() throws InterruptedException, UnsuspiciousException;

    /**
     * Remove the suitcase from the scanner.
     *
     * @return The suitcase on the scanner.
     */
    Suitcase get()
            throws InterruptedException;

    /**
     * Put a suitcase on the scanner.
     *
     * @param suitcase the suitcase to put on
     * @throws OverloadException if there is already a suitcase on the scanner
     */
    void put(Suitcase suitcase)
            throws OverloadException, UnsuspiciousException;

}
