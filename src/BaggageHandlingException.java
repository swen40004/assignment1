/**
 * A super class for all exceptions that occur in the baggage control.
 *
 * @author Tim Miller, Nicole Ronald
 */
abstract public class BaggageHandlingException extends Exception {

    //the end-of-line character
    final static protected String EOL = System.getProperty("line.separator");

    /**
     * The constructor to be called from subclasses.
     */
    public BaggageHandlingException(String message) {
        super(message);
    }

    /**
     * Create a new exception with a given message, and optionally a
     * nested exception.
     */
    public BaggageHandlingException(String message, Throwable cause) {
        super(message, cause);
    }

}
