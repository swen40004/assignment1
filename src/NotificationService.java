import java.util.Observable;

/**
 * Responsible for capturing messages and allowing other classes to observe
 * them.
 *
 * @author Aram Kocharyan
 */
public class NotificationService extends Observable {

    private static NotificationService defaultService;

    /**
     * Notifies observers of an exception occurring. This is useful for
     * observing exceptions in different threads.
     *
     * @param e An Exception
     */
    public void notifyException(Exception e) {
        setChanged();
        notifyObservers(e);
    }

    /**
     * @return A global instance of the notification service.
     */
    public static NotificationService getDefault() {
        if (defaultService == null) {
            defaultService = new NotificationService();
        }
        return defaultService;
    }

}
