/**
 * A simple implementation of an {@link XRay}
 *
 * @author Aram Kocharyan
 */
public class SimpleXRay implements XRay {

    private Suitcase suitcase;

    @Override
    public void xRay(Suitcase suitcase) throws InterruptedException {
        this.suitcase = suitcase;
    }

    @Override
    public boolean needScan() {
        return suitcase != null && suitcase.isSuspicious();
    }

    @Override
    public void clearXRayFlag() {
        // I don't need to use this method. According to the discussion board
        // this is implementation specific, so it probably shouldn't be in the
        // interface. Rather, the code using the XRay would store a reference to
        // a SimpleXRay which contains the new methods specific to that
        // implementation.
    }

}
