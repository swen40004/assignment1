import java.util.logging.Logger;

/**
 * Produces {@link Suitcase} objects and places them on a {@link Belt} at
 * random intervals.
 *
 * @author Aram Kocharyan
 */
public class SuitcaseProducer extends RandomBeltThread {

    private static Logger log =
            Logger.getLogger(SuitcaseProducer.class.getName());

    public SuitcaseProducer(Belt belt) {
        super(belt);
        setInterval(4000);
    }

    public void execute() {
        Suitcase sc = Suitcase.getInstance();
        try {
            log.info("Trying to produce");
            getBelt().put(sc, 0);
            log.info("Produced");
        } catch (Exception e) {
            NotificationService.getDefault().notifyException(e);
        }
    }

}
