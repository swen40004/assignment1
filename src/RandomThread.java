import java.util.Random;
import java.util.logging.Logger;

/**
 * A Thread which executes an action at random intervals.
 *
 * @author Aram Kocharyan
 */
public abstract class RandomThread extends IntervalThread {

    private static Random rand = new Random();

    private static Logger log =
            Logger.getLogger(RandomThread.class.getName());

    public long getInterval() {
        Float interval = super.getInterval() * rand.nextFloat();
        return interval.longValue();
    }

    public void execute() {
        // Override in subclasses
        log.info("Executing");
    }

}
