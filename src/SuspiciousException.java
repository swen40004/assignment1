/**
 * An exception that occurs when a suspicious suitcase makes it to the end of
 * the belt not cleaned.
 *
 * @author Tim Miller, Nicole Ronald
 */
public class SuspiciousException extends BaggageHandlingException {
    /**
     * Create a new SuspiciousException.
     */
    public SuspiciousException(String message) {
        super(message);
    }
}
