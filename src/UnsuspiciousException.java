/**
 * An exception that occurs when the scanner is commanded to scan a suitcase
 * that is already clean.
 *
 * @author Tim Miller, Nicole Ronald
 */
public class UnsuspiciousException extends BaggageHandlingException {
    /**
     * Create a new UnsuspiciousException.
     */
    public UnsuspiciousException(String message) {
        super(message);
    }
}
