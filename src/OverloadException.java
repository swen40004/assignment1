/**
 * An exception that occurs when a suitcase is damaged.
 *
 * @author Tim Miller, Nicole Ronald
 */
public class OverloadException extends BaggageHandlingException {

    // TODO this should be called DamagedSuitcaseException,
    // since it can be used for when two suitcases are placed on top of one another,
    // or when a suitcase falls off the edge.

    /**
     * Create a new OverloadException with a specified message.
     */
    public OverloadException(String message) {
        super(message);
    }

}
