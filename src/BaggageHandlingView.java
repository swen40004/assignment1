import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.imageio.ImageIO;
import java.io.File;

/**
 * The view class for the factory simulator.
 *
 * @author Tim Miller, Nicole Ronald
 */
public class BaggageHandlingView extends JPanel {
    // the frame for this view
    protected JFrame frame;

    // the belts, filler, and robot arms from which this view reads
    protected Belt belt;
    protected RobotArms robotArms;
    protected Scanner scanner;

    // the pictures of the suitcases
    protected Image normalSuitcaseImage;
    protected Image suspiciousSuitcaseImage;
    protected Image cleanSuitcaseImage;

    // the length of the side of a box
    final private static int BOX_SIDE = 120;

    // positions for the boxes.
    // LB = left belt. RB = Right Belt. SC = scanner.
    final private static int LB1_X = 40;
    final private static int LB1_Y = 40;
    final private static int LB2_X = LB1_X + BOX_SIDE;
    final private static int LB2_Y = LB1_Y;
    final private static int LB3_X = LB2_X + BOX_SIDE;
    final private static int LB3_Y = LB1_Y;
    final private static int LB4_X = LB3_X + BOX_SIDE;
    final private static int LB4_Y = LB1_Y;
    final private static int LB5_X = LB4_X + BOX_SIDE;
    final private static int LB5_Y = LB1_Y;

    final private static int RB1_X = LB1_X;
    final private static int RB1_Y = LB1_Y + 3 * BOX_SIDE;
    final private static int RB2_X = RB1_X + BOX_SIDE;
    final private static int RB2_Y = RB1_Y;
    final private static int RB3_X = RB2_X + BOX_SIDE;
    final private static int RB3_Y = RB1_Y;
    final private static int RB4_X = RB3_X + BOX_SIDE;
    final private static int RB4_Y = RB1_Y;
    final private static int RB5_X = RB4_X + BOX_SIDE;
    final private static int RB5_Y = RB1_Y;

    final private static int SC_X = LB1_X;
    final private static int SC_Y = LB3_Y + (int) (1.5 * BOX_SIDE);

    // position and size of the robot arms
    final private static int ARM_WIDTH = 20;
    final private static int ARM_LENGTH = (int) (1.5 * BOX_SIDE) + ARM_WIDTH
            / 2;
    final private static int ARM_X = LB3_X + (BOX_SIDE / 2) - ARM_WIDTH / 2;
    final private static int ARM_Y = LB1_Y + (2 * BOX_SIDE) - ARM_WIDTH / 2;

    // offsets for drawing suitcases in robot arms
    public static final int ARM_HORIZONTAL_OFFSET = 50;
    public static final int ARM_VERTICAL_OFFSET = 50;

    // position of the robot motor
    final private static int ROBOT_X = LB3_X + (BOX_SIDE / 4);
    final private static int ROBOT_Y = SC_Y + (BOX_SIDE / 4);

    // the size of the canvas
    final private static int CANVAS_X = LB5_X + BOX_SIDE + RB1_X;
    final private static int CANVAS_Y = LB1_Y + BOX_SIDE + (2 * RB1_Y);

    // labels
    final private static int LB_LABEL_X = LB2_X;
    final private static int LB_LABEL_Y = LB2_Y + BOX_SIDE + 20;
    final private static String LB_LABEL_TEXT = "Baggage belt";

    final private static int RB_LABEL_X = RB2_X;
    final private static int RB_LABEL_Y = RB2_Y - 10;
    final private static String RB_LABEL_TEXT = "Right belt";

    final private static int SC_LABEL_X = SC_X + (BOX_SIDE / 4);
    final private static int SC_LABEL_Y = SC_Y + BOX_SIDE + 20;
    final private static String SC_LABEL_TEXT = "Scanner";

    final private static int ROBOT_LABEL_X = ARM_X - 80;
    final private static int ROBOT_LABEL_Y = SC_Y + (BOX_SIDE / 2);
    final private static String ROBOT_LABEL_TEXT = "Robot";

    // image locations
    final private static String NORMAL_SUITCASE_IMAGE = "image/suitcase.png";
    final private static String SUSPICIOUS_SUITCASE_IMAGE =
            "image/blank_suitcase.png";
    final private static String CLEAN_SUITCASE_IMAGE =
            "image/suitcase_purple.png";

    /**
     * Create a new view from the factory components.
     */
    public BaggageHandlingView(JFrame frame, Belt belt, Scanner scanner,
                               RobotArms robotArms) {
        this.belt = belt;
        this.scanner = scanner;
        this.robotArms = robotArms;

        frame.setSize(CANVAS_X, CANVAS_Y);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
    }

    /**
     * Update the view using a specified graphics component.
     */
    public void update(Graphics graphics) {
        paint(graphics);
    }

    /**
     * Paint the view canvas using a specified graphics component.
     */
    public void paint(Graphics graphics) {
        Graphics2D g = (Graphics2D) graphics;

        // draw the background
        Dimension dim = getSize();
        g.setColor(getBackground());
        g.fillRect(0, 0, dim.width, dim.height);

        // draw the labels
        Font font = new Font("Arial", Font.PLAIN, 20);
        g.setFont(font);
        g.setColor(Color.BLACK);
        g.drawString(LB_LABEL_TEXT, LB_LABEL_X, LB_LABEL_Y);
        g.drawString(SC_LABEL_TEXT, SC_LABEL_X, SC_LABEL_Y);
        g.drawString(ROBOT_LABEL_TEXT, ROBOT_LABEL_X, ROBOT_LABEL_Y);

        // draw the left belt
        g.setColor(Color.WHITE);
        g.fill3DRect(LB1_X, LB1_Y, BOX_SIDE, BOX_SIDE, true);
        g.fill3DRect(LB2_X, LB2_Y, BOX_SIDE, BOX_SIDE, true);
        g.fill3DRect(LB3_X, LB3_Y, BOX_SIDE, BOX_SIDE, true);
        g.fill3DRect(LB4_X, LB2_Y, BOX_SIDE, BOX_SIDE, true);
        g.fill3DRect(LB5_X, LB3_Y, BOX_SIDE, BOX_SIDE, true);

        // draw the scanner
        g.fill3DRect(SC_X, SC_Y, BOX_SIDE, BOX_SIDE, true);

        // draw the suitcases
        checkImage();
        // left belt suitcases
        for (int i = 0; i < belt.maxSize(); i++) {
            Suitcase suitcase = belt.peek(i);
            if (suitcase != null) {
                int x = LB1_X + (i * BOX_SIDE);
                drawSuitcase(g, suitcase, x, LB1_Y);
            }
        }

        // Draw suitcase on scanner if present.
        if (scanner.peek() != null) {
            drawSuitcase(g, scanner.peek(), SC_X, SC_Y);
        }

        // draw the robot arms; first, the motor
        g.setColor(Color.RED);
        g.fillOval(ROBOT_X, ROBOT_Y, BOX_SIDE / 2, BOX_SIDE / 2);

        g.setColor(Color.BLACK);


        // nronald: draw robot arms and suitcases -- this is very approximate
        // but does the job
        if (robotArms.getPosition() == Position.ONE) {

            // suitcase
            if (robotArms.getArmSuitcase() != null) {
                drawSuitcase(g, robotArms.getArmSuitcase(), SC_X, SC_Y
                        - ARM_LENGTH);
            }
            // arm
            g.fill3DRect(ARM_X - 2 * BOX_SIDE + ARM_WIDTH * 2, ARM_Y
                    - (BOX_SIDE * 2) + (ARM_WIDTH * 3), ARM_WIDTH, ARM_LENGTH,
                    true);
            g.fill3DRect(ARM_X - ARM_LENGTH, ARM_Y - ARM_WIDTH / 2, ARM_LENGTH,
                    ARM_WIDTH, true);
        } else if (robotArms.getPosition() == Position.TWO) {

            // suitcase
            if (robotArms.getArmSuitcase() != null) {
                drawSuitcase(g, robotArms.getArmSuitcase(), SC_X + BOX_SIDE,
                        SC_Y - ARM_LENGTH);
            }
            // arm
            g.fill3DRect(ARM_X - BOX_SIDE + ARM_WIDTH, ARM_Y - (BOX_SIDE * 2)
                    + (ARM_WIDTH * 3), ARM_WIDTH, ARM_LENGTH, true);
            g.fill3DRect(ARM_X - ARM_LENGTH / 2, ARM_Y - ARM_WIDTH / 2,
                    ARM_LENGTH / 2, ARM_WIDTH, true);
        }

        // Draw arms and everything attached to them
        else if (robotArms.getPosition() == Position.THREE) {
            // added by mkaspar: draw suitcase being held by arm
            if (robotArms.getArmSuitcase() != null) {
                drawSuitcase(g, robotArms.getArmSuitcase(),
                        SC_X + 2 * BOX_SIDE, SC_Y - ARM_LENGTH);
            }
            g.fill3DRect(ARM_X, ARM_Y - (BOX_SIDE * 2) + (ARM_WIDTH * 3),
                    ARM_WIDTH, ARM_LENGTH, true);
        } else if (robotArms.getPosition() == Position.FOUR) {

            if (robotArms.getArmSuitcase() != null) {
                drawSuitcase(g, robotArms.getArmSuitcase(),
                        SC_X + 3 * BOX_SIDE, SC_Y - ARM_LENGTH);
            }
            g.fill3DRect(ARM_X + BOX_SIDE - ARM_WIDTH, ARM_Y - (BOX_SIDE * 2)
                    + (ARM_WIDTH * 3), ARM_WIDTH, ARM_LENGTH, true);
            g.fill3DRect(ARM_X + ARM_WIDTH, ARM_Y - ARM_WIDTH / 2,
                    ARM_LENGTH / 2, ARM_WIDTH, true);
        } else if (robotArms.getPosition() == Position.FIVE) {

            if (robotArms.getArmSuitcase() != null) {
                drawSuitcase(g, robotArms.getArmSuitcase(),
                        SC_X + 4 * BOX_SIDE, SC_Y - ARM_LENGTH);
            }
            g.fill3DRect(ARM_X + 2 * BOX_SIDE - 2 * ARM_WIDTH, ARM_Y
                    - (BOX_SIDE * 2)
                    + (ARM_WIDTH * 3), ARM_WIDTH, ARM_LENGTH, true);
            g.fill3DRect(ARM_X + ARM_WIDTH, ARM_Y - ARM_WIDTH / 2, ARM_LENGTH,
                    ARM_WIDTH, true);
        } else if (robotArms.getPosition() == Position.SCANNER) {

            if (robotArms.getArmSuitcase() != null) {
                drawSuitcase(g, robotArms.getArmSuitcase(), SC_X, SC_Y);
            }
            g.fill3DRect(ARM_X - ARM_LENGTH, ARM_Y - ARM_WIDTH / 2, ARM_LENGTH,
                    ARM_WIDTH, true);

        }
    }

    // if there are no image objects, load them
    private void checkImage() {
        if (suspiciousSuitcaseImage == null) {
            suspiciousSuitcaseImage = openImage(SUSPICIOUS_SUITCASE_IMAGE);
        }

        if (normalSuitcaseImage == null) {
            normalSuitcaseImage = openImage(NORMAL_SUITCASE_IMAGE);
        }

        if (cleanSuitcaseImage == null) {
            cleanSuitcaseImage = openImage(CLEAN_SUITCASE_IMAGE);
        }
    }

    // open an image file and return the Image instance.
    private Image openImage(String filename) {
        Image image = null;
        try {
            image = ImageIO.read(new File(filename));
        } catch (java.io.IOException e) {
            // exit if the file cannot be opened.
            System.err.println("File " + filename + " cannot be opened");
            System.exit(1);
        }

        return image;
    }

    // draw the suitcase in the box at coordinates (x, y), centering in the box
    private void drawSuitcase(Graphics2D g, Suitcase suitcase, int x, int y) {

        Image image = normalSuitcaseImage;
        if (suitcase.isSuspicious()) {
            if (suitcase.isClean()) {
                image = cleanSuitcaseImage;
            } else {
                image = suspiciousSuitcaseImage;
            }
        }

        int imageWidth = image.getWidth(null);
        int imageHeight = image.getHeight(null);

        int x_offset = x + ((BOX_SIDE - imageWidth) / 2);
        int y_offset = y + ((BOX_SIDE - imageHeight) / 2);

        g.drawImage(image, x_offset, y_offset, null);
    }
}
