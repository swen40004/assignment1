/**
 * The possible states for the robot arms.
 * ONE, TWO, THREE, FOUR, FIVE: at the relevant position on the belt
 * SCANNER: at the scanner
 *
 * @author Nicole Ronald
 */
public enum Position {

    // NOTE: this class causes a lot of coupling throughout the interface
    // implementations, which may be undesirable if they are to be reused.

    SCANNER, ONE, TWO, THREE, FOUR, FIVE;

    public static Position valueFromOrdinal(int ordinal) {
        return Position.values()[ordinal];
    }

}
