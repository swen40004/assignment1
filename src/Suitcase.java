import java.util.*;

/**
 * A class representing a suitcase moving through baggage control.
 *
 * @author Tim Miller, Nicole Ronald, Aram Kocharyan
 */
public class Suitcase {
    protected static Random r = new Random();

    //specifies whether the suitcase needs checking
    protected boolean suspicious = false;

    //specifies whether the suitcase is clean
    protected boolean clean = true;

    //the ID of this suitcase
    protected int id;

    //the next ID to be allocated
    protected static int nextId = 0;

    private volatile double suspiciousProb;

    //create a new suitcase with a given ID
    private Suitcase(int id) {
        this.id = id;
        setSuspiciousProb(0.1);
        if (r.nextFloat() < suspiciousProb) {
            suspicious = true;
            clean = false;
        }
    }

    /**
     * Get a new Suitcase instance with a unique ID.
     */
    public static Suitcase getInstance() {
        return new Suitcase(nextId++);
    }

    /**
     * @return the id of this suitcase.
     */
    public int getId() {
        return id;
    }

    /**
     * Mark this suitcase as clean.
     */
    public void clean() {
        clean = true;
    }

    /**
     * @return true if and only if this suitcase is marked as suspicious.
     */
    public boolean isSuspicious() {
        return suspicious;
    }

    /**
     * Mark this suitcase as suspicious.
     */
    public void setSuspicious() {
        suspicious = true;
    }

    /**
     * @return true if and only if this suitcase is marked as clean.
     */
    public boolean isClean() {
        return clean;
    }

    /**
     * @return The probability a suitcase is suspicious and not clean.
     */
    public double getSuspiciousProb() {
        return suspiciousProb;
    }

    /**
     * {@link Suitcase#getSuspiciousProb}
     * Precondition: suspiciousProb >= 0 && suspiciousProb <= 1
     */
    public void setSuspiciousProb(double suspiciousProb) {
        assert suspiciousProb >= 0 && suspiciousProb <= 1;
        this.suspiciousProb = suspiciousProb;
    }

    public String toString() {
        return "Suitcase(" + id + ")";
    }
}
