import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: Aram
 * Date: 20/03/13
 * Time: 10:28 PM
 * To change this template use File | Settings | File Templates.
 */

public class MonitorTest {

    static A a;

    private static Logger log =
            Logger.getLogger(MonitorTest.class.getName());

    public static void main(String args[]) {
        a = new A();

        // Sleeping before setting a breakpoint seems to prevent debugger
        // issue with threads.

        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        Thread b = new Thread() {
            public void run() {
                try {
                    Thread.sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                log.info("thread");
            }
        };
        b.start();

        synchronized (a) {
            log.info("begin");
            try {
                a.wait();
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            log.info("end");
        }
    }

}
