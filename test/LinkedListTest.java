import java.util.LinkedList;

/**
 * Simple tests on a LinkedList.
 */
public class LinkedListTest {

    public static void main(String[] args) {
        LinkedList<Integer> list = new LinkedList<Integer>();
        for (int i = 0; i < 5; i++) {
            list.add(i, i);
        }
        list.addFirst(6);
        list.removeLast();
    }

}
