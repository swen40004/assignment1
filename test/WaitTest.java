import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: Aram
 * Date: 25/03/13
 * Time: 2:12 AM
 * To change this template use File | Settings | File Templates.
 */
public class WaitTest {

    private static Logger log =
            Logger.getLogger(WaitTest.class.getName());

    final Thread a;
    final Thread b;
    final Thread c;

    public WaitTest() {

        a = (new Thread(new Runnable() {
            public synchronized void run() {
//                while (true) {
                try {
                    Thread.sleep(2000);
                    log.info("a");
                    notifyAll();
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
//                }
            }
        }));

        b = (new Thread(new Runnable() {
            public void run() {
                while (true) {
                    try {
                        log.info("b before");
                        synchronized (a) {
                            log.info("b before in");
                            a.wait();
                            log.info("b after in");
                        }
                        log.info("b after");
                    } catch (InterruptedException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
        }));

        c = (new Thread(new Runnable() {
            public void run() {
                try {
                    log.info("c before");
                    synchronized (a) {
                        log.info("c before in ");
                        a.wait();
                        log.info("c after in");
                    }
                    log.info("c after");
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }));

        a.start();
        b.start();
        c.start();

    }

    public static void main(String args[]) {
        WaitTest a = new WaitTest();
    }

}
