import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: Aram
 * Date: 24/03/13
 * Time: 11:58 PM
 * To change this template use File | Settings | File Templates.
 */
public class InterruptedExceptionTest {

    private static Logger log =
            Logger.getLogger(InterruptedExceptionTest.class.getName());

    public static class TestThread extends Thread {
        public TestThread(Runnable runnable) {
            super(runnable);
        }

        public void test() {
            log.info("test");
        }
    }

    public static void main(String args[]) {

        Runnable scan = new Runnable() {
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    Thread.currentThread().interrupt();
                    return;
                }
                log.info("123");
            }
        };

        TestThread thread = (new TestThread(scan));
        thread.start();
        thread.interrupt();
        thread.test();

    }


}
