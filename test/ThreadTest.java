import java.util.concurrent.AbstractExecutorService;
import java.util.concurrent.Executor;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.logging.Logger;

/**
 * Created with IntelliJ IDEA.
 * User: Aram
 * Date: 22/03/13
 * Time: 11:25 PM
 * To change this template use File | Settings | File Templates.
 */
public class ThreadTest {

    private static Logger log =
            Logger.getLogger(ThreadTest.class.getName());

    public static void main(String args[]) {

        Runnable scan = new Runnable() {
            public void run() {
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                log.info("123");
            }
        };

        Thread thread = (new Thread(scan));
        thread.start();
        log.info("synch");

        try {
            thread.join();
            log.info("456");
            thread = (new Thread(scan));
            thread.start();
            thread.join();
            log.info("789");
        } catch (InterruptedException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

}
