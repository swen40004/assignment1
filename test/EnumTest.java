
public class EnumTest {

    public enum SomeEnum {
        VAL0, VAL1, VAL2
    }

    public static void main(String[] args) {

        SomeEnum e = SomeEnum.VAL1;
        System.out.println(e);

        System.out.println(SomeEnum.valueOf(e.name()));
        System.out.println(SomeEnum.values()[2]);
        System.out.println(e.ordinal());

    }

}
